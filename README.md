
---

**WARNING!**: This is the *Old* source-code repository for the PQuantifier application needed for the quantization of phosphoproteomic data for the web-app at [www.LymPHOS.org](https://www.LymPHOS.org).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/lymphosweb/pquantifier_code/) located at https://sourceforge.net/p/lp-csic-uab/lymphosweb/pquantifier_code/**  

---
  
  
# LymPHOS2 PQuantifier

LymPHOS2 PQuantifier application needed for the quantization of phosphoproteomic data.  
  
  
---

**WARNING!**: This is the *Old* source-code repository for the PQuantifier application needed for the quantization of phosphoproteomic data for the web-app at [www.LymPHOS.org](https://www.LymPHOS.org).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/lymphosweb/pquantifier_code/) located at https://sourceforge.net/p/lp-csic-uab/lymphosweb/pquantifier_code/**  

---
  
