#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Calculates intensity distributions for TMT or iTRAQ reporter ions
from integrator json output files
"""

import json
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
from zipfile import ZipFile
#
#
def get_qintens(jdata):
    qintens = list()
    for scan in jdata.values():
        scankeys = scan.keys()
        for jkey in ('itraq_info','tmt_info'):
            if jkey in scankeys:
                qintens.extend( [float(each)
                                 for each in scan[jkey].split('|')] )
                break
    return qintens
#
#
def draw_hist(xvalues, log=False):
    """"""
    if log:
        xmax = np.log10(max(xvalues))
        xvalues = [np.log10(item) for item in xvalues if item > 0]
        bins = 100
        title = 'log values'
    else:
        xmax = max(xvalues)
        bins = 1000
        title = ''

    plt.xlim(0, xmax)
    plt.hist(xvalues, bins = bins)
    plt.xlabel(title)
    plt.show()
#
#
def print_hist_values(xvalues):
    count_values = list2hist(xvalues)
    for each in sorted(count_values)[1:50]:
        print('{0} : {1}'.format(each, count_values[each]))
#
#
def list2hist(xvalues):
    a_dict = defaultdict(int)
    for value in xvalues:
        a_dict[value] += 1
    return a_dict
#
#
#
if __name__ == '__main__':
    filenames = ["datafiles/01_exp1_Cvs4h_iTRAQ_RESULTS_NewDB.zip",
                 "datafiles/05_exp5_Cvs15minvs2h_OGE_RESULTS_NewDB.zip",
                 ]
    for filename in filenames:
        print(filename)
        zip_file = ZipFile(filename, 'r')
        name = zip_file.namelist()[0]
        json_file = zip_file.open(name, 'r')
        json_data = json.load(json_file)
        qintens = get_qintens(json_data)
        print_hist_values(qintens)        
        draw_hist(qintens)