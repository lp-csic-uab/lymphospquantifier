#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Joaquin'

import json
import re
import logging
import logging.config
import config
from pq_model import Peptide, Experiment
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from zipfile import ZipFile, BadZipfile
#
#
logging.config.fileConfig(config.log_file)
log3 = logging.getLogger('fill_tables')
#
#
def _quant_field(pep_orig):
    if '737' in pep_orig:
        return 'tmt_info'
    elif '214' in pep_orig:
        return 'itraq_info'
    else:
        return None


# noinspection PyShadowingNames
def fill_experiments(datafile, session):
    """Fills SqlAlchemy-sqlite table experiment.

    datafile: json archive with experiment data (can be zipped).
    session: an open sqlite session.

    """
    json_data = json.load(open_zip(datafile))

    for values in json_data.values():
        experiment = Experiment(experiment_id=int(values['experiment_id']),
                                type_id=int(values['type_id']),
                                label=values['label'],
                                quant_series=values['quant'])
        session.add(experiment)
#
#
# noinspection PyShadowingNames
def fill_peptide(datafile, session):
    """Fills SqlAlchemy-sqlite table peptide.

    datafile: json archive with Integrator result data (can be zipped).
    session: an open sqlite session.

    """
    json_data = json.load(open_zip(datafile))
    log3.info('Start DB Import of file: %s' % datafile)
    for key, values in json_data.iteritems():
        if ('engine' in key) or ('params' in key):
            continue
        # Verify that peptide in json_data matches some real protein:
        # - not null
        if not values['consensus_proteins']:
            log3.debug('** No consensus_proteins for peptide %s in %s **'
                       % (values['consensus_mod'], key))
            continue
        # - not decoys
        if not [each for each in values['consensus_proteins'].split('|') 
                if 'decoy' not in each]:
            log3.debug('** Only decoy consensus_proteins for peptide %s in %s **'
                       % (values['consensus_mod'], key))
            continue
        
        (arch, fs, ls) = key.split('.')
        ascore_data = values.get('ascore_data', None)
        phosphorylated = bool(ascore_data)

        ms_level = values['ms']
        if ascore_data:
            sequence_mod = ascore_data['peptide']
        else:
            sequence_mod = values['consensus_mod']
        sequence = values['consensus']
        # get sequence for dict keys: I(737)DEGHSNS(23)PR  -->  IDEGHSNsPR
        sequence_phos_key = set_p_amino_in_consensus(sequence_mod, sequence)
        
        # Get quantitative data if peptide is labeled, and there is quantitative
        # data:
        quant_field = _quant_field(sequence_mod)
        quant_info = values.get(quant_field, '0')
#        try:
#            quant_info = values['tmt_info']
#        except KeyError:
#            try:
#                quant_info = values['itraq_info']
#            except KeyError:
#                quant_info = ''

        if ms_level == '3':
            precursor_data = values.get('ms2_spectral_info', None)
            if precursor_data:
                ms2_file = precursor_data["ms2_file"]
                (_, prec_fs, _) = ms2_file.split('.')
            else:
                prec_fs = 0
                log3.info("ms3 scan %5s, %-30s, %s  no precursor"
                          % (fs, sequence_mod, arch))
        else:
            prec_fs = 0

        peptide = Peptide(raw_file=arch,
                          sequence_mod=sequence_mod,
                          sequence_phos_key=sequence_phos_key,
                          scan=int(fs),
                          ms_level=ms_level,
                          prec_scan=int(prec_fs),
                          phosphorylated=phosphorylated,
                          quant_info=quant_info,
                          experiment_id=int(values['experiment']),
                          )
        session.add(peptide)
    log3.info('End DB Import of file: %s' % datafile)
#
#
def open_zip(fpath):
    """Returns a file object from a zipped file set for reading.

    If the file fpath is not a .zip, it tries to open it as a normal .txt file.

    """
    try:
        azip = ZipFile(fpath)
        # Get a handle for the first compressed file
        handle = azip.open(azip.namelist()[0])
    except IOError as exception:
        log3.critical('%s : %s' % (exception.strerror, fpath))
        raise exception
    except BadZipfile as exception:
        log3.info('%s : %s' % (exception.args[0], fpath))
        try:
            handle = open(fpath)
        except IOError as exception:
            log3.critical('%s : %s' % (exception.strerror, fpath))
            raise exception
    return handle
#
#
def set_p_amino_in_consensus(sequence_mod, sequence):
    """Replace p-amino acids with lower-case in consensus sequences.

    Uses consensus_mod sequences <sequence_mod> as a pattern to convert upper-
    case S,T,Y amino acids to lower case in consensus sequences <sequence>
    when they appear as phosphorylated in <sequence_mod>.

    sequence_mod = "I(737)DK(737)LSS(21)JSR",
    sequence =     "IDKLSSJSR"

    returns ->      "IDKLSsJSR",

    """
    sequence = list(sequence)
    pos = 0
    strings = re.split('[(), ]', sequence_mod)
    for string in strings:
        if string.isdigit():
            if string in ('21', '23'):
                sequence[pos - 1] = sequence[pos - 1].lower()
        else:
            pos += len(string)
    return ''.join(sequence)
#
#
#
if __name__ == '__main__':

    engine = create_engine(config.DATABASE, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()
    fill_experiments(config.experiment_data, session)
    for archive in config.datafiles:
        fill_peptide(archive, session)
    session.commit()
    log3.info('%s : Table Filling Finished' % config.DATABASE)
