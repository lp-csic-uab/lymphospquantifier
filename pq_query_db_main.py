#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
:synopsis: Reads quantification data from SQLite database, processes it, and
produces a JSON file ready to be uploaded to LymPHOS DataBase.

This (pq_query_db_main.py) is the Main Script of the package It was developed
from pq_query_db.py to include non-phosphorylated peptides in the output.

** pq_query_db.py
this script produces a JSON file containing ratios and standard deviations for
each p-peptide scan, and a statistical evaluation of the change given by the
calculated ratio. These ratio values are tested taking into consideration the
normal distribution of non-phosphorylated peptides for each experiment.
It contains also the average ratio of all the peptide scans in a 
supra-experiment.

** pq_query_db_main.py
non-phosphorylated peptides were needed for inclusion in LymPHOS DB for future
exports to mzIdentML files for their use in the Sp-HPP project.
Previously, significance calculations (T-Student tests) were carried out after
data from PQuantifier was stored in LymPHOS using and independent script
(LymPHOS Tools/exportLymPHOSDB2CSVReport.py) and the results provided in an
excel-like file.
Now those T-Student change significance calculations are also performed by this
PQuantifier script.


*** Some general characteristics of the Data Processing ****

- assignation of missing values:
    The original program assigned a minimum, constant value for the intensity
    of the reporters ions. The value of the constant depends on the labeling
    (defaults used have been 5 for iTRAQ and 250 or 600 for TMT).
    On new versions there are two more methods of assignation:
        - Assign the minimum non-zero value observed in the experiment for each 
          reporter ion.
        - Assign the average of the 0.1% of the lower non-zero values in the 
          experiment for each reporter ion.
    Assignation affects missing values as well as reporter signals with
    intensities lower than the established or calculated cutoff (on DanteR data, 
    only missing values are reassigned).

- Number crunching for ratio calculation is performed taking into account error 
  transmission (using the uncertainties package).
  However, average ratios and corresponding stdevs are directly calculated from 
  the nominal values of the peptide ratios.

*** Some Nomenclature ***

eid :
 experiment id. Includes several injections (fractions)
 a.k.a. experiment

type_id :
 several experiment with the same conditions.
 a.k.a Supra-experiment
 a.k.a Condition
 a.k.a Condition id


:created:    2013-09-06

:authors:    Joaquín Abián (jabianmonux@gmail.com) and Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015 LP-CSIC/UAB. All rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__AUTHOR__ = 'Joaquin'
__VERSION__ = '1.3'
__UPDATED__ = '2015-10-20'


#===============================================================================
# Imports
#===============================================================================
import os
import numpy
import json
import logging
import logging.config
import uncertainties as unc
from collections import defaultdict, OrderedDict
from uncertainties import ufloat, AffineScalarFunc
from pq_model import Peptide, Experiment
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy import and_
from pq_analyze import get_distribution, log_convert, make_figures, mix_normals, degrees_freedom, ttest, tstudent


#===============================================================================
# Global variables
#===============================================================================
import config


logging.config.fileConfig(config.log_file)
log = logging.getLogger('main')


#===============================================================================
# Class definitions
#===============================================================================
class UncertaintyEncoder(json.JSONEncoder):
    """A hook for json to save uncertainties in list format"""

    def default(self, obj):
        if isinstance(obj, AffineScalarFunc):
            return [obj.nominal_value, obj.std_dev]
        return json.JSONEncoder.default(self, obj)


#===============================================================================
# Function definitions
#===============================================================================
def get_partial_data_dict(experiments, save=False, draw=False):   #REFACTORIZE
    """Build dict with data for each scan stored in a SQLite database.
    
    :param list experiments: list of experiments IDs to processs.
    :param bool save: save histogram and normal fit to disk?
    :param bool draw: show histogram and normal fit on screen?
    
    :return tuple : 
    - A dict of dict of list of dict, with keys:
          dictionary[sequence_phos_key][experiment_id]
      and values which are list of dictionaries with following keys:
          data['file']   -> raw, raw filename
              ['scan']   -> scan, scan number
              ['values'] -> p_ratio, quant ratios (AffineScalarFunc objects)
              ['flag_error'] -> p_flag, flag indicating correction
              ['flag_signif'] -> flag_signif, -1, 0, +1
              ['average'] -> type_id, condition/supra-experiment ID
    - A dict of dict of dict: no-phospho statistics (for further T-test) for 
      each experiment grouped by supra-experiment. Ex.:
      {type_id: {exp_id: {'mu':0.0, 'sigma':nop_sigma, 'n':number of nop_spectra}, ... }, ... }
    - A dict of dict of dict of list: peptides stdvs (for further stdv > 3*mean-stdv
      warnings), grouped by supra-experiment, experiment and stage/time. Ex.:
      {type_id: {exp_id: {stage: [stdvs, ...], ... }, ... }, ... }
    """
    session = Session()
    database = session.query(Peptide).join(Experiment)
    min_signals = config.min_signals
    assign_method = config.assign_method
    #
    partial = defaultdict( lambda: defaultdict(list) )
    type2exp2nop_stats = dict() #{type_id: {exp_id: {'mu':0.0, 'sigma':nop_sigma, 'n':number of nop_spectra}, ... }, ... }
    type2exp2stage2stdvs = defaultdict( lambda: defaultdict( lambda: defaultdict(list) ) )
    # for each experiment ID:
    for eid in experiments:
        log.info('Processing Experiment: %s' % eid)
        # Prepare synchronized lists of lists and dictionaries:
        p_infos = []            #Meta-information for phospho(p) and no-phospho(n) peptides
        n_infos = []            #
        phospho = OrderedDict() #
        nphospho = OrderedDict()#
        p_quants = [] #Quantitative-information for phospho(p) and no-phospho(n) peptides
        n_quants = [] #
        #
        deleted_keys = []
        # for each set of SCX fractions analyzed.
        #
        # Get ms2 spectra:
        for item in database.filter( and_(Experiment.experiment_id == eid,
                                          Peptide.ms_level == '2') ):
            key = item.raw_file, item.scan
            quant_info = [ float(x) for x in item.quant_info.split('|') ]
            # Eliminates spectra with less than min_signals tag signal or
            # without quantitative data (if no quantitative data =>
            # item.quant_info = '0'  =>  quant_info = [0.0]  =>  non_zeros < min_signals)
            non_zeros = sum(1 for item in quant_info if item > 0)
            if non_zeros < min_signals:
                log.debug('****bad_ or no_quant ms2****: %s -> qdata: %s' % (item.sequence_mod, item.quant_info))
                deleted_keys.append(key)
                continue

            if item.phosphorylated:
                phospho[key] = [item.sequence_phos_key, quant_info]
                p_quants.append(quant_info)
            else:
                nphospho[key] = [item.sequence_phos_key, quant_info]
                n_quants.append(quant_info)
        n_ms2phospho = len(phospho.keys())
        log.info('*** %i phospho ms2 spectra ***' % n_ms2phospho)
        #
        # Get ms3 spectra (only for phosphorylated):
        deleted = 0
        redundant = 0
        for item in database.filter(and_(Experiment.experiment_id == eid,
                                         Peptide.ms_level == '3',
                                         Peptide.phosphorylated == True)):
            prec_key = item.raw_file, item.prec_scan

            if prec_key in deleted_keys:
                deleted += 1
                continue
            if prec_key in phospho:
                redundant += 1
                continue

            key = item.raw_file, item.scan
            quant_info = [float(x) for x in item.quant_info.split('|')]
            # Eliminates spectra with less than min_signals tag signal or
            # without quantitative data (item.quant_info='' -> quant_info=[]):
            non_zeros = sum(1 for item in quant_info if item > 0)
            if non_zeros < min_signals:
                log.debug('****bad_ or no_quant ms3****: %s -> qdata: %s' % (item.sequence_mod, item.quant_info))
                deleted_keys.append(key)
                continue

            phospho[key] = [item.sequence_phos_key, quant_info]
            p_quants.append(quant_info)

        log.info('*** %i ms3 spectra with redundant ms2 ***' % redundant)
        log.info('*** %i ms3 spectra with no_quant ms2 ***' % deleted)
        n_ms3phospho = len(phospho.keys()) - n_ms2phospho
        log.info('*** %i phospho ms3 spectra ***' % n_ms3phospho)

        # key: raw_file, scan
        # values: sequence_phos_key, quant_info
        for key, values in phospho.iteritems():
            p_infos.append([values[0], key[0], key[1]])
        for key, values in nphospho.iteritems():
            n_infos.append([values[0], key[0], key[1]])

        log.info('*** %i phospho spectra ***' % len(p_quants))
        log.info('*** %i normal spectra ***' % len(n_quants))

        exp_db = session.query(Experiment).filter(Experiment.experiment_id == eid).first()
        type_id = exp_db.type_id
        series = exp_db.quant_series
        # [0, 0, 60, 60, 120, 120] <= "0|0|60|60|120|120"
        series = [int(item) for item in series.split('|')]
        # [0, 60, 120]  <<==  [0, 0, 60, 60, 120, 120]
        stages = get_stages_in_experiment(series)
        cutoff = config.cutoff[exp_db.label]
        log.info('*** default missing value assignation: %i ***' % cutoff)
        log.info('*** assign method for missing values: %s ***' % assign_method)

        p_quants, p_flags = get_avg_quants(p_quants, series, stages, cutoff, assign_method)
        n_quants, n_flags = get_avg_quants(n_quants, series, stages, cutoff, assign_method)

        p_ratios, p_flags = get_ratios(p_quants, stages, p_flags)
        n_ratios, n_flags = get_ratios(n_quants, stages, n_flags)

        if not (p_ratios and n_ratios):
            log.warning('ERROR no quantitative data in experiment %s' % eid)
            continue
        #
        lg_p_ratios = log_convert(p_ratios)
        lg_n_ratios = log_convert(n_ratios)
        # Get no-phosphorilated peptides statistics:
        n_distribution = get_distribution(lg_n_ratios, 'no phospho ratios')
        (_, nmu, nsigma, _, _) = n_distribution
        # Collect no-phosphorilated peptides statistics (for further T-test),
        # grouped by supra-experiment (`type_id`) and experiment(`eid`):
        type2exp2nop_stats[type_id] = { eid: { 'mu': 0.0, 'sigma':  nsigma, 
                                               'n':  len(n_quants) } }
        # Get significance relative to mu of no_phospho:
        psignif_flags = mark_significance(lg_p_ratios, nmu, nsigma)
        nsignif_flags = mark_significance(lg_n_ratios, nmu, nsigma)
        # Correct ALL ratios with no_phospho offset (mu):
        centered_lg_p_ratios = center_mu(lg_p_ratios, nmu)
        centered_lg_n_ratios = center_mu(lg_n_ratios, nmu) #TEST!!!!
        #
        p_params = (p_infos, centered_lg_p_ratios, p_flags, psignif_flags)
        n_params = (n_infos, centered_lg_n_ratios, n_flags, nsignif_flags)
        for params in (p_params, n_params):
            for info, ratio, flag, signif in zip(*params):
                sequence_phos_key, raw, scan = info
                #
                data = {'file': raw,
                        'scan': scan,
                        'values': ratio,
                        'stages': stages,
                        'flag_error': flag,
                        'flag_signif': signif,
                        'average': type_id,
                        }
                #
                partial[sequence_phos_key][eid].append(data)
                # Collect peptides stdvs (for further stdv > 3*mean-stdv
                # warnings), grouped by supra-experiment (`type_id`),
                # experiment(`eid`) and stage:
                stage2stdvs = type2exp2stage2stdvs[type_id][eid]
                for stage, affinescalar_ratio in zip(stages[1:], ratio):
                    stage2stdvs[stage].append(affinescalar_ratio.std_dev)
        #
        if draw or save:
            p_distribution = get_distribution(lg_p_ratios, 'phospho ratios')
            # noinspection PyUnboundLocalVariable
            make_figures(eid,
                         ('phospho', lg_p_ratios, p_distribution),
                         ('no phospho', lg_n_ratios, n_distribution),
                         draw=draw, save=save,
                         dir_save=config.output_directory)
    #
    return partial, type2exp2nop_stats, type2exp2stage2stdvs
#
#
def get_average_data_dict(partial_data, type2nop_stats, type2stage2meanstdvx3):
    """Build dict with summarized sequence data from a partial dictionary. Also 
    add T-test information, and stdv > 3*mean-stdv warnings.
    
    :param dict of dict of list of dict partial_data: Ex. 
    {"NHQEEDLTEFLCANHVLK": {29: [ {"scan": 4958, 
                                   "file": "29_IMAC_PI_0_15_120_20120110_Fr37_120130164317", 
                                   "average": 4, 
                                   "stages": [0, 15, 120],
                                   "values": [ [0.24000826335374947+/-0.1467], 
                                               [0.5570135758128263+/-0.1471] ], 
                                   "flag_signif": [0, 1], 
                                   "flag_error": [0, 0]
                                   }, 
                                 ], 
                            30: [ {"scan": 4653, 
                                   "file": "30_IMAC_PI_0_15_120_20120110_Fr37", 
                                   "average": 4, 
                                   "stages": [0, 15, 120], 
                                   "values": [ [0.07251026243711986+/-0.3291], 
                                               [0.30135510221448936+/-0.0200] ], 
                                   "flag_signif": [0, 0], 
                                   "flag_error": [0, 0]
                                   }, 
                                 ], 
                            },
    }
    :param dict of dict type2nop_stats: 
    {type_id: {'mu':0.0, 'sigma':nop_sigma, 'n':number of nop_spectra}, ... }
    :param dict of dict type2stage2meanstdvx3: mean peptides stdvs x 3, grouped 
    by supra-experiment and stage/time (for adding stdv > 3*mean-stdv warnings). 
    Ex.: {type_id: {stage: mean peptide stdvs x 3, ... }, ... }
    
    :result dict of dict : Ex.
    {"NHQEEDLTEFLCANHVLK": {4: {"members": [ [29, "29_IMAC_PI_0_15_120_20120110_Fr37_120130164317", 4958], 
                                             [30, "30_IMAC_PI_0_15_120_20120110_Fr37", 4653] ], 
                                "stages": [0, 15, 120], 
                                "values": [ [0.15625926289543468, 
                                             0.083749000458314807], 
                                            [0.42918433901365782, 
                                             0.12782923679916847] ], 
                                "flag_signif": [ [0, 2, 0], [0, 1, 1] ],
                                "flag_error": [0, 0]
                                
                                "tstat": [0.79694659479899688, 
                                          2.562267272855995], 
                                "tstudent95": [12.328532832393151, 
                                               12.545270221285184], 
                                "tstudent99": [60.515348820957755, 
                                               62.310709634174181],
                                "ttestpassed95": [0, 0], 
                                "ttestpassed99": [0, 0], 
                                
                                "warn_sdmeanx3": [0, 0]}
                                }, 
                            },
    }
    """
    average = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    for seq_key, seq_partial_data in partial_data.iteritems(): #seq_key is sequence_phos_key
        # Collect data to average, grouped by sequence and supra-experiment: 
        for experiment_id, experiment_data in seq_partial_data.iteritems():
            type_id = experiment_data[0]['average']
            stages = experiment_data[0]['stages']
            
            for spectrum in experiment_data:
                member = [experiment_id, spectrum['file'], spectrum['scan']]

                average[seq_key][type_id]['members'].append(member)
                average[seq_key][type_id]['values'].append(spectrum['values'])
                average[seq_key][type_id]['flag_error'].append(spectrum['flag_error'])
                average[seq_key][type_id]['flag_signif'].append(spectrum['flag_signif'])
                
            average[seq_key][type_id]['stages'] = stages
        # Average collected and grouped data:
        # TODO convert to dictionary to accept experiments of the same type
        # TODO with different timings
        # values = [[10+/-4, 20+/-2], [15+/-3, 15+/-2], etc]
        for type_id, average_data in average[seq_key].items(): #X-NOTE: Changed for in-situ modification of average_data dictionary.
            values = average_data['values']
            errors = average_data['flag_error']
            signif = average_data['flag_signif']
            # - Summarize significance:
            signif = summarize_significance(signif)
            # - Average ratios and sum flags for a sequence:
            if len(values) > 1: #There is the minimum data to do statistics
                nominal_values = [map(unc.nominal_value, item) for item in values]
                values = numpy.array(nominal_values)
                mean_values = numpy.mean(values, 0)
                stdevs = numpy.std(values, 0)
                values = zip(mean_values, stdevs)
                errors = list(numpy.sum(errors, 0))
                # - Get extra T-Student test data for significance validation:
                tstudent_data = t_student_tests(values, 
                                                len( average_data['members'] ), 
                                                type2nop_stats[type_id])
            else: #Not enough data to do statistics
                values = [ [value.nominal_value, value.std_dev] 
                           for value in values[0] ]
                errors = errors[0]
                tstudent_data = {'tstat': [], #FIX with more convenient 'null values'?
                                 'tstudent95': [], 
                                 'tstudent99': [], 
                                 'ttestpassed95': [], 
                                 'ttestpassed99': []}
            # - Add stdv > 3*mean-stdv warnings (1 -> warning, 0 -> No problem):
            warnsdmeanx3 = [ 0 if stdv < type2stage2meanstdvx3[type_id][stage] else 1
                             for stage, (_, stdv) in zip(average_data['stages'][1:], values) ]
            #
            average_data['values'] = values
            average_data['flag_error'] = errors
            average_data['flag_signif'] = signif
            average_data.update(tstudent_data) #Add extra T-Student test data.
            average_data['warn_sdmeanx3'] = warnsdmeanx3
    #
    return average
#
#
def summarize_significance(sign_list):
    """Summarizes series of individual significance data in a list of occurrences

    For a group of p.e. 5 measurements and two different states, the input data
    has the form:

    sign_list = [[-1, 1],
                 [0, 1],
                 [0, 0],
                 [0,-1],
                 [0,-1]]

    where -1, 0, 1  indicates decrease, no change or increase respectively.
    The result is a list of 3 items lists indicating how many measurements
    decrease, do not change or increase (as list items 0,1,2 respectively)
    for each state:

    returns: [[1,4,0], [2,1,2]]

    """
    # noinspection PyUnresolvedReferences
    return [[state.count(flag) for flag in (-1, 0, 1)] for state in zip(*sign_list)]
#
#
def t_student_tests(values, n_spectra, nop_stats):
    """
    Calculate T-Student test data for significance validation of a peptide in 
    a supra-experiment.
    
    :param list values: list of mean ratio, std deviation pairs.
    :param n_spectra: number of spectra used for the averages.
    :param dict: {'mu':0.0, 'sigma':nop_sigma, 'n':number of nop_spectra}
    
    :return defaultdict : T-Student test data for change significance validation.
    """
    ttests_data = defaultdict(list)
    for ratio, stdv in values:
        # Student's t-test for mean log2ratio of 2 populations (no
        # phosphorilated, and current peptide in current condition in
        # current time) with unequal sample sizes, unequal variance:
        df = degrees_freedom(nop_stats['mu'],    ratio, 
                             nop_stats['sigma'], stdv, 
                             nop_stats['n'],     n_spectra)
        tstat = ttest(nop_stats['mu'],    ratio, 
                      nop_stats['sigma'], stdv, 
                      nop_stats['n'],     n_spectra)
        tstudent95 = tstudent(0.05, df)
        tstudent99 = tstudent(0.01, df)
        # Add current time/condition T-Student test data to dictionary:
        ttests_data['tstat'].append(tstat)
        ttests_data['tstudent95'].append(tstudent95)
        ttests_data['tstudent99'].append(tstudent99)
        ttests_data['ttestpassed95'].append( 0 if tstat < tstudent95 else 1 ) # 1 -> significative change, 0 -> no significative change
        ttests_data['ttestpassed99'].append( 0 if tstat < tstudent99 else 1 ) #
    #
    return ttests_data
#
#
def center_mu(pratios, offset):
    """Corrects ratios of p-peptides with np-peptides distribution offset.

    assumes:
    mu np-peptides = 0  -> np-peptides are not changed by activation.
                        -> offset reflects labelling errors.
    """
    centered = []
    for row in pratios:
        centered.append( [ratio - offset for ratio in row] )

    return centered
#
#
def mark_significance(ratios, mu, sigma, n_sigmas=2):
    """Generates flags indicating significant changes for a given sigma.

    ratios: list of lists of ratios (log). One ratio per condition.
    n_sigmas: number of sigmas for the significance window.
    mu, sigma correspond to a normal distribution of the log2 of the ratios.

    """
    low_sd = mu - sigma * n_sigmas
    high_sd = mu + sigma * n_sigmas

    log.info('low_sd, high_sd  %.5f  %.5f' % (low_sd, high_sd))

    signif_flags = []
    for row in ratios:
        row_flags = []
        for ratio in row:
            if ratio > high_sd:
                row_flags.append(1)
            elif ratio < low_sd:
                row_flags.append(-1)
            else:
                row_flags.append(0)
        signif_flags.append(row_flags)

    return signif_flags
#
#
def get_avg_quants(quant_lols, series, stages, cutoff=5, mode='constant'):
    """Averages values in quant_lols columns corresponding to the same state.

    Before averaging, assigns missing values and values lower than a minimum value.
    Assignation can be done
         - using a user-provided <cutoff> minimum value or
         - using different methods to calculate this cutoff
    During assignation, each time an original value is modifiesd, a flag count is increased for this value

    This procedure packs 3 functions:
     calculate_cuttoff .- Provides several modes of determining the minimum value or values to set
     set_cutoff .- Set the cutoff
     average_columns .- Averages data from replicates (columns corresponding to the same state)

    """
    cutoffs = calculate_cutoff(quant_lols, cutoff, mode)
    quant_lols, flag_lols = set_cutoff(quant_lols, cutoffs)
    return average_columns(quant_lols, flag_lols, series, stages)
#
#
def get_ratios(data, stages, my_flags):
    """Calculates ratios and sum flags between items in each list of a two
    lists of lists (data and my_flags).

    For each list in a list of lists (data) calculates the ratios
    between the items in the list and a specific item in that list, the control.
    Each item in the list corresponds to a given class, always in the same order
    in all lists. The position of  each class is given by stages.
    stage 0 indicates the position of the control value.

    my_flags have the same dimensions of data and its items take integer
    values. They indicate the number of problems the corresponding value in
    data had during its calculation (original values lower than a
    threshold value). The resulting flags list of lists compiles these
    individual flags for each pair of operated items in data (each ratio).

    data = [[3.0, 7.0, 11.0],
            [2.0, 6.0, 10.0]
            ]
    my_flags = [[1,0,1],
               [0,1,0]
               ]
    stages = [0, 60, 120]

    gives:
    ratios = [[2.33, 3.67],
              [3.0, 5.0]
              ]
    flags = [[1, 2],
             [1, 0]
             ]

    """
    ratios = []  # total number of conditions in the essay
    flags = []
    control = stages.index(0)  # control will be always first 0
    conditions = [stages.index(item) for item in stages]
    conditions.pop(control)  # remove the first zero from conditions

    faults = 0
    for spectrum, my_flag in zip(data, my_flags):  # [quant1,..,quant4]
        new_ratios = []
        new_flags = []

        if len(my_flag) < 2:
            faults += 1
            if faults <= 20:
                log.warning(
                    'ERROR len of flags %s < 2 for quant=%s with conditions %s' % (
                        str(my_flag),
                        str(spectrum),
                        str(conditions)))
            continue

        for condition in conditions:
            ratio = spectrum[condition] / spectrum[control]
            flag = my_flag[condition] + my_flag[control]
            new_ratios.append(ratio)
            new_flags.append(flag)

        ratios.append(new_ratios)
        flags.append(new_flags)

    if faults > 20:
        log.warning('%i faulty entries' % faults)

    return ratios, flags
#
#
def calculate_cutoff(listoflists, cutoff, mode):
    """Determine a cutoff value for each column on an array (lols) on the distribution of values in column.

    Approaches:
    'constant'  .- Use a fixed value <cutoff> provided in config.py
    'minimum'   .- Use the minimum non-zero value for each data column (for each reporter ion)
    'mean_0.1%' .- Use the average of the 0.1% of the lowest non-zero values in each data column.
                   if less than 3000 scans with value (less than 3 values to average), average the 3 lower values
                   It is the default calculation if mode is not 'constant' or 'minimum'.

    """
    if mode == 'constant':
        cutoffs = [cutoff for _ in listoflists[0]]
    else:
        ar = numpy.array(listoflists)
        art = ar.transpose()
        if mode == 'minimum':
            cutoffs = [min(row[numpy.nonzero(row)]) for row in art]
        else:
            cutoffs = []
            for row in art:
                rowi = sorted(row[numpy.nonzero(row)])
                values2avg = int(0.001 * len(rowi))
                if values2avg < 3:
                    result = numpy.average(rowi[:3])
                else:
                    result = numpy.average(rowi[:values2avg])
                cutoffs.append(result)
    log.info('missing values assignation for mode <%s> : %s' % (mode, [int(i) for i in cutoffs]))
    return cutoffs
#
#
def set_cutoff(listoflists, cutoffs):
    """Assign minimum values to the zeros and under-cutoff values in a list of lists.
    """
    new_lol = []
    flags = []
    for row in listoflists:
        new_row = []
        flags_row = []
        for value, min_value in zip(row, cutoffs):
            if value < min_value:
                new_row.append(min_value)
                flags_row.append(1)
            else:
                new_row.append(value)
                flags_row.append(0)
        new_lol.append(new_row)
        flags.append(flags_row)

    return new_lol, flags
#
#
def average_columns(quant_lols, flag_lols, series, stages):
    """Combines replicate values <quant_lols> and flags <flag_lols> of the same time stage.

    Values are averaged and flags (which indicate a reassigned value) summed.

    series = [0, 0, 60, 60, 120, 120] (or [0, 0, 60, 120, 60, 120])
    stages = [0, 60, 120]

    the helping dictionary <averaged> will be:
    averaged = {0: [0, 1]
                1: [2, 3]
                2: [4, 5]
                }

    Then, for input data like

    quant_lols = [
                  [5, 5, 6, 8, 10, 12],
                  [5, 5, 5, 7, 9, 11],
                  ..................
                 ]

    flags = [
             [1 , 1 , 0, 0, 0, 0]
             [1 , 1 , 0, 0, 0, 0]
             .....................
             ]


    Averaged values and summed flags will be:
    lol_averaged = [
                   [5.0, 7.0, 11.0],
                   [5.0, 6.0, 10.0],
                   ................
                   ]

    flags will be:
    lol_flags = [
                [2 , 0 , 0]
                [2 , 0 , 0]
                ]
    """
    stat_num = len(stages)

    averaged = {}
    for col, stage in enumerate(stages):
        groups = [idx for idx, item in enumerate(series) if item == stage]
        averaged[col] = groups

    log.debug('stages %s' % str(stages))
    log.debug('series %s' % str(series))
    log.debug('averaged %s' % str(averaged))

    quant_lol_avg = []
    flags_lol_sum = []  # indicates some problem in the calculation
    for qrow, frow in zip(quant_lols, flag_lols):
        new_row = [0] * stat_num  # do not let empty spaces in case there are more times...
        new_flags = [0] * stat_num
        for col, idxs in averaged.items():
            values = []
            flags = []
            for idx in idxs:
                value = qrow[idx]
                flag =  frow[idx]
                values.append(value)
                flags.append(flag)
            mean_value = numpy.mean(values, 0)
            std_dev = numpy.std(values, 0)
            value = ufloat(mean_value, std_dev)
            new_row[col] = value
            new_flags[col] = sum(flags)

        quant_lol_avg.append(new_row)
        flags_lol_sum.append(new_flags)

    return quant_lol_avg, flags_lol_sum
#
#
def get_stages_in_experiment(series):
    """Determines the different states in a series of states.

    The different states are integers and are returned in sorted order
    series = [0, 0, 60, 120, 60, 120]  -> [0, 60, 120]

    """
    # This can not be done using a list comprehension because <stages> in being filled 'in vivo'
    stages = []
    for item in series:
        if item not in stages:
            stages.append(item)

    return sorted(stages)
#
#
def combine(partial, average):
    """Build a new dictionary with input dictionaries as branches.

     The two input dictionaries have the same keys which are used for the
     primary key of the new dictionary

    """
    combined = {}
    for sequence_phos_key in partial:
        combined[sequence_phos_key] = {'partials': partial[sequence_phos_key],
                                       'average': average[sequence_phos_key],
                                       }
    return combined
#
#
#
if __name__ == '__main__':
    engine = create_engine(config.DATABASE, echo=False)
    Session = sessionmaker(bind=engine)
    #
    # noinspection PyArgumentEqualDefault
    partials = get_partial_data_dict(config.experiments, 
                                     draw=config.graph_draw,
                                     save=config.graph_save)
    partial_data_dict, type2exp2nop_stats, type2exp2stage2stdvs = partials
    # Mix no-phospho experiment normals for each supra-experiment:
    type2nop_stats = {type_id: mix_normals(exp2nop_stats) 
                      for type_id, exp2nop_stats in type2exp2nop_stats.items()}
    # Sumarize stdv data for each supra-experiment and stage/time. This is:
    # from {type_id: {exp_id: {stage: [stdvs, ...], ... }, ... }, ... } to 
    # {type_id: {stage: mean peptide stdvs x 3, ... }, ... } :
    type2stage2meanstdvx3 = dict()
    for type_id, exp2stage2stdvs in type2exp2stage2stdvs.items():
        stage2allexp_stdvs = defaultdict(list)
        for stage2stdvs in exp2stage2stdvs.values():
            for stage, stdvs in stage2stdvs.items():
                stage2allexp_stdvs[stage].extend(stdvs)
        type2stage2meanstdvx3[type_id] = { stage: numpy.mean(allexp_stdvs) * 3
                                           for stage, allexp_stdvs 
                                           in stage2allexp_stdvs.items() }
    #
    average_data_dict = get_average_data_dict(partial_data_dict, 
                                              type2nop_stats, 
                                              type2stage2meanstdvx3)
    #
    combined_data_dict = combine(partial_data_dict, average_data_dict)
    #    
    root = config.output_directory
    json_file = os.path.join(root, config.output_name) + '.json'
    json.dump(combined_data_dict, open(json_file, 'w'), indent=4,
              cls=UncertaintyEncoder)
    #
    log.info('Finished processing of experiments. See File : %s' % json_file)
