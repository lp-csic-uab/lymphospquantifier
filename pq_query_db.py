#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Reads quantification data from sqlite database and produces a json file ready
to be uploaded to LymPHOS DataBase
"""

__author__ = 'Joaquin'

import os
import numpy
import json
import logging
import logging.config
import uncertainties as unc
import config
from collections import defaultdict
from uncertainties import ufloat, AffineScalarFunc
from pq_model import Peptide, Experiment
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy import and_
from pq_analyze import get_distribution, log_convert, make_figures
#
#
logging.config.fileConfig(config.log_file)
log = logging.getLogger('main')
#
#
class UncertaintyEncoder(json.JSONEncoder):
    """A hook for json to save uncertainties in list format"""
    def default(self, obj):
        if isinstance(obj, AffineScalarFunc):
            return [obj.nominal_value, obj.std_dev()]
        return json.JSONEncoder.default(self, obj)
#
#
def get_partial_data_dict(experiments, save=False, draw=False):
    """Build dict with data for each scan stored in a sqlite database.

    returns: dictionary of dictionaries with keys:
                  dictionary[sequence_phos_key][experiment_id]
             and values which are list of dictionaries with keys:
                  data['file']   -> raw, raw filename
                      ['scan']   -> scan, scan number
                      ['values'] -> p_ratio, quant ratios
                      ['flag_error'] -> p_flag, flag indicating correction
                      ['flag_signif'] -> flag_signif, -1, 0, +1
                      ['average'] -> type_id, condition id
    """
    session = Session()
    partial = defaultdict(lambda: defaultdict(list))
    database = session.query(Peptide).join(Experiment)
    # for each condition type
    for eid in experiments:
        infos = []
        p_quants = []
        n_quants = []

        log.info('Processing Experiment: %s' % eid)
        # for each set of SCX fractions analyzed.
        #
        # get ms2 spectra
        deleted_keys = []
        phospho = {}
        min_signals = config.min_signals
        for item in database.filter(and_(Experiment.experiment_id == eid,
                                         Peptide.ms_level == '2')):

            quant_info = [float(x) for x in item.quant_info.split('|')]
            # eliminates spectra with less than min_signals tag signal
            non_zeros = sum(1 for item in quant_info if item > 0)
            if non_zeros < min_signals:
                log.debug('****bad_ or no_quant****: %s' % item.sequence_mod)
                key = item.raw_file, item.scan
                deleted_keys.append(key)
                continue

            if item.phosphorylated:
                key = item.raw_file, item.scan
                phospho[key] = [item.sequence_phos_key, quant_info]
            else:
                n_quants.append(quant_info)

        log.info('*** %i phospho ms2 spectra ***' % len(phospho.keys()))
        #
        # get ms3 spectra
        deleted = 0
        redundant = 0
        for item in database.filter(and_(Experiment.experiment_id == eid,
                                         Peptide.ms_level == '3',
                                         Peptide.phosphorylated == True)):
            prec_key = item.raw_file, item.prec_scan

            if prec_key in deleted_keys:
                deleted += 1
                continue
            if prec_key in phospho:
                redundant += 1
                continue
            else:
                quant_info = [float(x) for x in item.quant_info.split('|')]
                key = item.raw_file, item.scan
                phospho[key] = [item.sequence_phos_key, quant_info]

        log.info('*** %i ms3 spectra with redundant ms2 ***' % redundant)
        log.info('*** %i ms3 spectra with no_quant ms2 ***' % deleted)

        # key: raw_file, scan
        # values: sequence_phos_key, quant_info
        for key, values in phospho.iteritems():
            p_quants.append(values[1])
            infos.append([values[0], key[0], key[1]])

        log.info('*** %i phospho spectra ***' % len(p_quants))
        log.info('*** %i normal spectra ***' % len(n_quants))

        exp_db = session.query(Experiment).filter(Experiment.experiment_id == eid).first()
        series = exp_db.quant_series
        type_id = exp_db.type_id
        # [0, 0, 60, 60, 120, 120] <= "0|0|60|60|120|120"
        series = [int(item) for item in series.split('|')]
        # [0, 60, 120]  <<==  [0, 0, 60, 60, 120, 120]
        stages = get_stages_in_experiment(series)
        cutoff = config.cutoff[exp_db.label]
        print '****', cutoff
        p_quants, p_flags = average_columns(p_quants, series, stages, cutoff)
        n_quants, n_flags = average_columns(n_quants, series, stages, cutoff)

        p_ratios, p_flags = get_ratios(p_quants, stages, p_flags)
        n_ratios, n_flags = get_ratios(n_quants, stages, n_flags)
        
        if not (p_ratios and n_ratios):
            log.warning('ERROR no quantitative data in experiment %s' % eid)
            continue

        lg_p_ratios = log_convert(p_ratios)
        lg_n_ratios = log_convert(n_ratios)

        n_distribution = get_distribution(lg_n_ratios, 'no phospho ratios')
        if draw or save:
            p_distribution = get_distribution(lg_p_ratios, 'phospho ratios')

        (_, n_mu, n_sigma, _, _) = n_distribution
        # significance relative to mu of no_phospho
        signif_flags = mark_significance(lg_p_ratios, n_mu, n_sigma)

        # correct ratios with no_phospho offset (mu)
        centered_lg_p_ratios = center_mu(lg_p_ratios, n_mu)

        for info, p_ratio, p_flag, flag_signif in zip(infos, centered_lg_p_ratios,
                                                      p_flags, signif_flags):

            sequence_phos_key, raw, scan = info

            data = {'file': raw,
                    'scan': scan,
                    'values': p_ratio,
                    'stages': stages,
                    'flag_error': p_flag,
                    'flag_signif': flag_signif,
                    'average': type_id,
                    }

            partial[sequence_phos_key][eid].append(data)

        if draw or save:
            # noinspection PyUnboundLocalVariable
            make_figures(eid,
                         ('phospho', lg_p_ratios, p_distribution),
                         ('no phospho', lg_n_ratios, n_distribution),
                         draw=draw, save=save,
                         dir_save=config.output_directory)

    return partial
#
#
def get_average_data_dict(partial):
    """Build dict with combined sequence data from a partial dictionary.

    """
    average = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    # seq_key is sequence_phos_key
    for seq_key, seq_partial_data in partial.iteritems():
        for experiment_id, experiment_data in seq_partial_data.iteritems():
            type_id = experiment_data[0]['average']
            stages = experiment_data[0]['stages']

            for spectrum in experiment_data:
                members = [experiment_id, spectrum['file'], spectrum['scan']]

                average[seq_key][type_id]['members'].append(members)
                average[seq_key][type_id]['values'].append(spectrum['values'])
                average[seq_key][type_id]['flag_error'].append(spectrum['flag_error'])
                average[seq_key][type_id]['flag_signif'].append(spectrum['flag_signif'])

            average[seq_key][type_id]['stages'] = stages
        
        # TODO convert to dictionary to accept experiments of the same type
        # TODO with different timings
        # values = [[10+/-4, 20+/-2], [15+/-3, 15+/-2], etc]
        for type_id in average[seq_key]:
            values = average[seq_key][type_id]['values']
            errors = average[seq_key][type_id]['flag_error']
            signif = average[seq_key][type_id]['flag_signif']

            # summarize significance
            signif = summarize_significance(signif)

            # average ratios and sum flags for a sequence
            if len(values) > 1:
                nominal_values = [map(unc.nominal_value, item) for item in values]
                values = numpy.array(nominal_values)
                mean_values = numpy.mean(values, 0)
                stdevs = numpy.std(values, 0)
                values = zip(mean_values, stdevs)
                errors = list(numpy.sum(errors, 0))
            else:
                values = values[0]   # jsonencoder will take care of ufloat
                errors = errors[0]

            average[seq_key][type_id]['values'] = values
            average[seq_key][type_id]['flag_error'] = errors
            average[seq_key][type_id]['flag_signif'] = signif

    return average
#
#
def summarize_significance(sign_list):
    """Summarizes series of individual significance data in a list of occurrences

    For a group of p.e. 5 measurements and two different states, the input data
    has the form:

    sign_list = [[-1, 1],
                 [0, 1],
                 [0, 0],
                 [0,-1],
                 [0,-1]]

    where -1, 0, 1  indicates decrease, no change or increase respectively.
    The result is a list of 3 items lists indicating how many measurements
    decrease, do not change or increase (as list items 0,1,2 respectively)
    for each state:

    returns: [[1,4,0], [2,1,2]]

    """
    # noinspection PyUnresolvedReferences
    return [[state.count(flag) for flag in (-1, 0, 1)] for state in zip(*sign_list)]
#
#
def center_mu(p_ratios, offset):
    """Corrects ratios of p-peptides with np-peptides distribution offset.

    assumes:
    mu np-peptides = 0  -> np-peptides are not changed by activation.
                        -> offset reflects labelling errors.
    """
    centered = []
    for row in p_ratios:
        centered.append([ratio - offset for ratio in row])

    return centered
#
#
def mark_significance(ratios, mu, sigma, n_sigmas=2):
    """Generates flags indicating significant changes for a given sigma.

    ratios: list of lists of ratios (log). One ratio per condition.
    n_sigmas: number of sigmas for the significance window.
    mu, sigma correspond to a normal distribution of the log2 of the ratios.

    """
    low_sd = mu - sigma * n_sigmas
    high_sd = mu + sigma * n_sigmas

    log.info('low_sd, high_sd  %.5f  %.5f' % (low_sd, high_sd))

    signif_flags = []
    for row in ratios:
        row_flags = []
        for ratio in row:
            if ratio > high_sd:
                row_flags.append(1)
            elif ratio < low_sd:
                row_flags.append(-1)
            else:
                row_flags.append(0)
        signif_flags.append(row_flags)

    return signif_flags
#
#
def average_columns(quant_list, series, stages, cutoff=5):
    """Averages values in quant list columns corresponding to the same state.

    series = [0, 0, 60, 60, 120, 120] (or [0, 0, 60, 120, 60, 120])
    stages = [0, 60, 120]

    quant_list = [
                 [2, 4, 6, 8, 10, 12],
                 [1, 3, 5, 7, 9, 11]
                 ]
    should average:
    averaged = [[3.0,7.0,11.0],
                  [2.0,6.0,10.0]
                     ]
    """
    stat_num = len(stages)

    averaged = {}
    for col, stage in enumerate(stages):
        groups = [idx for idx, item in enumerate(series) if item == stage]
        averaged[col] = groups

    log.debug('stages %s' % str(stages))
    log.debug('series %s' % str(series))
    log.debug('averaged %s' % str(averaged))

    lol_averaged = []
    lol_flags = []                # indicates some problem in the calculation
    for row in quant_list:
        new_row = [0] * stat_num  # do not let empty spaces in case there are more times...
        new_flags = [0] * stat_num
        for col, idxs in averaged.items():
            values = []
            flag = 0
            for idx in idxs:
                value = row[idx]
                # we do not want very low values that can create problems
                if value < cutoff:
                    value = cutoff
                    flag += 1
                values.append(value)
            mean_value = numpy.mean(values, 0)
            std_dev = numpy.std(values, 0)
            value = ufloat((mean_value, std_dev))
            new_row[col] = value
            new_flags[col] = flag
        lol_averaged.append(new_row)
        lol_flags.append(new_flags)

    return lol_averaged, lol_flags
#
#
def get_ratios(data, stages, my_flags):
    """Calculates ratios and sum flags between items in each list of a two
    lists of lists (data and my_flags).

    For each list in a list of lists (data) calculates the ratios
    between the items in the list and a specific item in that list, the control.
    Each item in the list corresponds to a given class, always in the same order
    in all lists. The position of  each class is given by stages.
    stage 0 indicates the position of the control value.

    my_flags have the same dimensions of data and its items take integer
    values. They indicate the number of problems the corresponding value in
    data had during its calculation (original values lower than a
    threshold value). The resulting flags list of lists compiles these
    individual flags for each pair of operated items in data (each ratio).

    data = [[3.0, 7.0, 11.0],
                  [2.0, 6.0, 10.0]]
    my_flags = [[1,0,1],
               [0,1,0]]
    stages = [0, 60, 120]

    gives:
    ratios = [[2.33, 3.67],
              [3.0, 5.0]]
    flags = [[1, 2],
             [1, 0]]

    """
    ratios = []                    # total number of conditions in the essay
    flags = []
    control = stages.index(0)      # control will be always first 0
    conditions = [stages.index(item) for item in stages]
    conditions.pop(control)        # remove the first zero from conditions

    faults = 0
    for spectrum, my_flag in zip(data, my_flags):   # [quant1,..,quant4]
        new_ratios = []
        new_flags = []

        if len(my_flag) < 2:
            faults += 1
            if faults <= 20:
                log.warning(
                    'ERROR len of flags %s < 2 for quant=%s with conditions %s' % (
                        str(my_flag),
                        str(spectrum),
                        str(conditions)))
            continue

        for condition in conditions:
            ratio = spectrum[condition] / spectrum[control]
            flag = my_flag[condition] + my_flag[control]
            new_ratios.append(ratio)
            new_flags.append(flag)

        ratios.append(new_ratios)
        flags.append(new_flags)

    if faults > 20:
        log.warning('%i faulty entries' % faults)

    return ratios, flags
#
#
def get_stages_in_experiment(series):
    """Determines the different states in a series of states.

    The different states are integers and are returned in order
    series = [0, 0, 60, 120, 60, 120]  -> [0, 60, 120]
    
    """
    stages = []
    for item in series:
        if item not in stages:
            stages.append(item)

    return sorted(stages)
#
#
def combine(partial, average):
    """Build a new dictionary with input dictionaries as branches.

     The two input dictionaries have the same keys which are used for the
     primary key of the new dictionary

    """
    combined = {}
    for sequence_phos_key in partial:
        combined[sequence_phos_key] = {'partials': partial[sequence_phos_key],
                                       'average': average[sequence_phos_key],
                                       }
    return combined
#
#
#
if __name__ == '__main__':

    engine = create_engine(config.DATABASE, echo=False)
    Session = sessionmaker(bind=engine)

    # noinspection PyArgumentEqualDefault
    partial_data_dict = get_partial_data_dict(config.experiments, 
                                              draw=config.graph_draw,
                                              save=config.graph_save)
    average_data_dict = get_average_data_dict(partial_data_dict)
    
    combined_data_dict = combine(partial_data_dict, average_data_dict)
    root = config.output_directory
    json_file = os.path.join(root, config.output_name) + '.json'
    json.dump(combined_data_dict, open(json_file, 'w'), indent=4,
              cls=UncertaintyEncoder)
    
    log.info('Finished processing of experiments. See File : %s' % json_file)
