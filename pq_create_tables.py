#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Joaquin'

from config import DATABASE
from sqlalchemy import create_engine
from pq_model import Base

def create_tables(database):
    engine = create_engine(database, echo=True)
    metadata = Base.metadata
    metadata.create_all(engine)


if __name__ == '__main__':

    create_tables(DATABASE)

