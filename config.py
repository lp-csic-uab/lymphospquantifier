__author__ = 'Joaquin'

# Logger configuration file:
log_file = 'logger.conf'
#
# #################   model   #####################
# ##############  create tables  ##################
# ##############   fill_tables   ##################
#
# Integrator files to process (json):
#  - For Testing:
# datafiles = [
#              "test/30_Reduced_for_tests.db",
#              ]
#  - For LymPHOS:
datafiles = [
             "datafiles/01_exp1_Cvs4h_iTRAQ_RESULTS_NewDB.zip",
             "datafiles/02_exp2_Cvs4h_iTRAQ_RESULTS_NewDB.zip",
#             # Controls versus controls are not valid for quantification:
#             #"DataFiles/03_exp3_CvsC_iTRAQ_RESULTS_NewDB.zip",
#             #"DataFiles/04_exp4_CvsCvsC_OGE_RESULTS_NewDB.zip",
#             # Experiment 5 is a bad quantitative experiment (0|120|15|0|120|15, and a high mu for no-phospho):
#             "datafiles/05_exp5_Cvs15minvs2h_OGE_RESULTS_NewDB.zip",
             "datafiles/06_exp6_Cvs15minvs2h_SCX_RESULTS_NewDB.zip",
             "datafiles/07_exp7_Cvs15minvs2h_RESULTS_NewDB.zip",
             "datafiles/29_exp29_Cvs15minvs2h_RESULTS_TOTAL.zip",
             "datafiles/30_exp30_Cvs15minvs2h_RESULTS.zip",
             "datafiles/35_OPS_Ab_0_15_120_20130612_Fr00.db.zip",
#             # Experiment 36 has been renamed and modified from its previous bad name and experiment code as experiment 30:
             "datafiles/36_ofs_PI_0_15_120_Fr00.zip",
             ]
#  - For LymPHOS JM OLD (only IMAC):
# datafiles = [
#             "datafiles/201_ops_Ab_0_15_30_60_120_1440_Fr00.zip", 
#             "datafiles/202_ops_Ab_0_15_30_60_120_1440_Fr00.zip", 
#             "datafiles/203_ops_Ab_0_15_30_60_120_1440_Fr00.zip", 
#             "datafiles/204_ops_Ab_0_15_30_60_120_1440_Fr00.zip", 
#             "datafiles/205_ops_Ab_0_15_30_60_120_1440_Fr00.zip", 
#             "datafiles/206_ops_Ab_0_15_30_60_120_1440_Fr00.zip", 
#             ]
#  - For LymPHOS JM NEW (IMAC + TiO2):
#datafiles = [
#             "datafiles/201_eos_Ab_0_15_30_60_120_1440_Fr00_IMACnTiO2_3.idb", 
#             "datafiles/202_eos_Ab_0_15_30_60_120_1440_Fr00_IMACnTiO2_3.idb", 
#             "datafiles/203_eos_Ab_0_15_30_60_120_1440_Fr00_IMACnTiO2_3.idb", 
#             "datafiles/205_eos_Ab_0_15_30_60_120_1440_Fr00_IMACnTiO2_3.idb", 
#             "datafiles/206_eos_Ab_0_15_30_60_120_1440_Fr00_IMACnTiO2_3.idb", 
#             "datafiles/208_eos_Ab_0_15_30_60_120_1440_Fr00_IMACnTiO2_3.idb", 
#             "datafiles/209_eos_Ab_0_15_30_60_120_1440_Fr00.idb",
#             "datafiles/210_eos_Ab_0_15_30_60_120_1440_Fr00.idb",
#            ]
#
# Experiment info file (json):
experiment_data = "datafiles/experiments_latest.db"
#
# IDs of experiments to process:
#  - For Testing:
# experiments = [30]
#  - For LymPHOS:
experiments = [1, 2, 6, 7, 29, 30, 35, 36]
#  - For LymPHOS JM:
# experiments = [209, 210]
# Database name to store processed integrator data:
DATABASE = "sqlite:///pquantifier.sqlite"
#
# ###############   query_db   ####################
# #############   build output   ##################
#
# Want to see and/or save the distribution graphs ?:
graph_draw = False
graph_save = True
# Minimum possible signal in a spectrum. Default constant value to assign missing values:
cutoff = {'iTRAQ': 4, 'TMT': 600}
# Method to assign missing values ['constant' | 'minimum' | 'mean_0.1%']
assign_method = 'mean_0.1%'
# Minimum number of tag signals to consider the spectrum:
min_signals = 2
# Output file directory:
output_directory = 'resultfiles'
# Basename for json output files:
output_name = 'combined_126729303536'
