#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
:synopsis: Functions to calculate and draw gaussian distributions, and also to
calculate T-Student tests.

It defines :func:`mix_normals`, :func:`tstudent` , :func:`ttest` and
:func:`degrees_freedom`, cross-ported from `LymPHOS Tools/tools_commmons.py` at
revision 174 (2015-08-04).

:created:    2011-06-18

:authors:     Joaquín Abián (jabianmonux@gmail.com) and Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015 LP-CSIC/UAB. All rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    1.2
:updated:    2015-08-12
"""

#===============================================================================
# Imports
#===============================================================================
import os
import numpy
import json
import time
import logging
import math
import uncertainties as unc
from uncertainties import umath
from scipy import histogram, stats
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt


#===============================================================================
# Global variables
#===============================================================================
__author__ = 'Joaquin'

log2 = logging.getLogger('main.analyze')


#===============================================================================
# Function definitions
#===============================================================================
# noinspection PyShadowingNames,PyPep8Naming
def calc_normal(x, limits=(-10, 10)):
    """Calculates mu, sigma from a gaussian fit of the x distribution.

    Curve fitting produces wrong results for distributions with few values:
    bins with 0 frequency affects calculation. This must be addressed.
    
    """
    nbins = 600
    xmin, xmax = limits
    bins = numpy.linspace(xmin, xmax, nbins)

    n, _ = histogram(x, bins)
    diff = (bins[1] - bins[0]) / 2
    bin_centers = [a_bin + diff for a_bin in bins[:-1]]
    A, mu, sigma = curve_fit(gauss, bin_centers, n)[0]
    log2.info('scipy_fit, A: %.4f, mu: %.4f, sigma: %.4f' % (A, mu, sigma))

    # gauss curve_fit sometimes produces negative sigmas
    sigma = abs(sigma)

    return A, mu, sigma, limits, nbins
#
#
# noinspection PyShadowingNames,PyPep8Naming
def plot_gauss(A=2, mu=0, sigma=0.5, limits=None, resolution=100):
    """Plot gaussian distribution with parameters A, mu, sigma

    limits: tuple (xmin, xmax).
            if not given the curve covering 20 times sigma is drawn
    resolution: int. determines number of points
    """
    resolution = float(resolution)
    if not limits:
        lower = int(resolution * (mu - 20 * sigma))
        upper = int(resolution * (mu + 20 * sigma))
    else:
        lower = int(resolution * limits[0])
        upper = int(resolution * limits[1])

    x = [v / resolution for v in range(lower, upper)]

    # noinspection PyTypeChecker
    data = gauss(x, A, mu, sigma)
    plt.plot(x, data, linewidth=2)
#
#
# noinspection PyShadowingNames,PyPep8Naming
def gauss(x, A, mu, sigma):
    """Returns f(x) for a gaussian function.

    in the canonical equation A = 1 / (sigma * (2 * pi)**0.5)
    """
    return A * numpy.exp(-(x - mu) ** 2 / (2 * sigma ** 2))
#
#
def cut_fraction(x, fraction=99):
    """Eliminates outsiders/extremes from a collection of values.

    """
    sx = sorted(x)
    total = len(sx)
    part = total * fraction / 100.0
    to_cut = int((total - part) / 2)

    end_cut = -1 if to_cut == 0 else -to_cut

    log2.info('** %i outsiders eliminated at cutoff %f. **'
              % (2 * to_cut, fraction))

    return sx[to_cut:end_cut]
#
#
# noinspection PyUnusedLocal
def cut_stdev(x, std):
    """

    """
    pass
#
#
def log_convert(data):
    """Converts a 2D array of numbers to log base 2

    """
    log_data = []
    for row in data:
        # noinspection PyUnresolvedReferences
        data = [umath.log(item, 2) for item in row]
        log_data.append(data)

    return log_data
#
#
def flatten_list_of_lists(data):
    """Flattens a 2D array of numbers

    data: lol or 2D array of floats. can contain numbers with uncertainties

    """
    return [unc.nominal_value(item) for row in data for item in row]
#
#
# noinspection PyPep8Naming,PyShadowingNames
def make_figures(experiment, *args, **kwargs):
    """Draw and save matplotlib plots.

    args: plots to save. tuples (legend, mu, sigma)
    kwargs: draw: plot in screen. True/False
           save: save as png file. True/False

    """
    legend = []
    for name, values, dist in args:
        values = numpy.reshape(values, (1, -1))
        A, mu, sigma, (xmin, xmax), nbins = dist
        bins = numpy.linspace(xmin, xmax, nbins)
        plt.hist(values[0], bins, alpha=0.5, lw=0.4)
        plot_gauss(A, mu, sigma)
        legend.append('%s mu: %.4f, sigma: %.4f' % (name, mu, sigma))

    plt.title('Experiment %s' % experiment)
    plt.figtext(0.5, 0.8, '\n'.join(legend))

    if kwargs.get('save', False):
        directory = kwargs.get('dir_save', False)
        my_date = time.localtime()
        my_date = '%i_%i' % (my_date.tm_hour, my_date.tm_min)
        path = '%s_%s' % (experiment, my_date)
        if directory:
            path = os.path.join(directory, path)
        plt.savefig(path, dpi=600)

    if kwargs.get('draw', False):
        plt.show()
    else:
        plt.close()
#
#
def get_distribution(values, name=''):
    """Calculates mu, sigma (gaussian dist) for the data in a list of list

    values: list or a list of lists that will be flattened
    name: label for the data collection
    draw: True|False
    """
    log2.info('**** %s ****' % name)
    try:
        values = flatten_list_of_lists(values)
    # if not iterable...
    except TypeError:
        pass
    data = cut_fraction(values)

    return calc_normal(data)
#
#
def mix_normals(exp_normals, exp_pack=None):
    """
    X-NOTE: https://en.wikipedia.org/wiki/Mixture_density#Moments
    """
    if exp_pack:
        exps = exp_pack
    else:
        exps = exp_normals.keys()
    #
    mixed_normal = exp_normals[ exps[0] ]
    for exp in exps[1:]:
        n = mixed_normal['n'] + exp_normals[exp]['n']
        summed_p = mixed_normal['n'] / n
        otther_p = exp_normals[exp]['n'] / n
        mu = ( summed_p * mixed_normal['mu'] + 
               otther_p * exp_normals[exp]['mu'] )
        sigma = math.sqrt(summed_p * ( (mixed_normal['mu'] - mu)**2 + 
                                        mixed_normal['sigma']**2) + 
                          otther_p * ( (exp_normals[exp]['mu'] - mu)**2 + 
                                        exp_normals[exp]['sigma']**2)
                          )
        mixed_normal = {'mu': mu, 'sigma': sigma, 'n': n}
    return mixed_normal
#
#
def tstudent(alpha, df):
    """
    Return T values from a real t-student distribution, not a table of pre-
    calculated values.
    """
    T = stats.t.interval(1-alpha, df)[1]
    return T
#
#
def ttest(mean1, mean2, sd1, sd2, n1, n2):
    """
    This test also known as Welch's t-test is used only when the two population
    variances are assumed to be different (the two sample sizes may or may not
    be equal) and hence must be estimated separately.
    
    - http://en.wikipedia.org/wiki/Student%27s_t-test#Unequal_sample_sizes.2C_unequal_variance
    - Welch, B. L. (1947), "The generalization of "student's" problem when 
      several different population variances are involved.", Biometrika 34: 28-35
    """
    mean_dif_variance = (sd1 ** 2) / n1 + (sd2 ** 2) / n2
    mean_dif_sd = math.sqrt(mean_dif_variance)
    t = (mean1 - mean2) / mean_dif_sd
    return abs(t)
#
#
def degrees_freedom(mean1, mean2, sd1, sd2, n1, n2):
    """
    This is the Welch-Satterthwaite equation.
    
    - Satterthwaite, F. E. (1946), "An Approximate Distribution of Estimates of 
      Variance Components.", Biometrics Bulletin 2: 110-114, doi:10.2307/3002019
    """
    sd1n1 = (sd1 ** 2) / n1
    sd2n2 = (sd2 ** 2) / n2
    df = ((sd1n1 + sd2n2) ** 2) / ((sd1n1 ** 2) / (n1 - 1) + 
                                   (sd2n2 ** 2) / (n2 - 1))
    return df
#
#
#
if __name__ == '__main__':

    files = [("test/ph_ratios_4_test_normal.json", 'phospho'),
             ("test/np_ratios_4_test_normal.json", 'no phospho')
             ]

    mu = sigma = 0
    to_plot = []
    for arch, legend in files:
        ratios = json.load(open(arch))
        distribution = get_distribution(ratios, legend)
        (_, mu, sigma, _, _) = distribution
        to_plot.append((legend, ratios, distribution))

    print 'returns, mu: %.4f, sigma: %.4f' % (mu, sigma)

    make_figures(0, *to_plot, draw=True, save=False)
