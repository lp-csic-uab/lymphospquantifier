#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Joaquin'

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Boolean, String
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

Base = declarative_base()

class Peptide(Base):
    __tablename__ = 'peptides'

    raw_file = Column(String, primary_key=True)
    sequence_mod = Column(String)                         # consensus_mod
    sequence_phos_key = Column(String, primary_key=True)  # consensus_mod -> 0H->P + LymPHOS format
    scan = Column(Integer, primary_key=True)
    phosphorylated = Column(Boolean)
    ms_level = Column(String)
    prec_scan = Column(Integer)
    quant_info = Column(String)                           # tmt_info, itraq_info
    experiment_id = Column(Integer, ForeignKey('experiments.experiment_id'))

    experiments = relationship("Experiment",
                               backref=backref("peptides"),
                               order_by="Experiment.experiment_id")

    def __init__(self, raw_file,
                 sequence_mod, sequence_phos_key, 
                 scan, ms_level, prec_scan, phosphorylated, quant_info,
                 experiment_id):
        self.raw_file = raw_file
        self.sequence_mod = sequence_mod
        self.sequence_phos_key = sequence_phos_key
        self.scan = scan
        self.ms_level = ms_level
        self.prec_scan = prec_scan
        self.phosphorylated = phosphorylated
        self.quant_info = quant_info
        self.experiment_id = experiment_id

    def __repr__(self):
        # noinspection PyStringFormat
        return '<Peptide "%s" Scan %i  ms%s>' % (self.sequence_phos_key,
                                                 self.scan, self.ms_level)


class Experiment(Base):
    __tablename__ = 'experiments'

    experiment_id = Column(Integer, primary_key=True)
    type_id = Column(Integer)
    label = Column(String)
    quant_series = Column(String)

    def __init__(self, experiment_id, type_id, label, quant_series):
        self.experiment_id = experiment_id
        self.type_id = type_id
        self.label = label
        self.quant_series = quant_series

    def __repr__(self):
        # noinspection PyStringFormat
        return '<Experiment "%i" label "%s" series "%s">' % (self.experiment_id,
                                                             self.label,
                                                             self.quant_series)
