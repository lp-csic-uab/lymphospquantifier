#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Joaquin'

import unittest
import uncertainties as unc
from pq_query_db import average_columns, get_ratios
from pq_query_db import get_stages_in_experiment
from pq_fill_tables import set_p_amino_in_consensus

# ########################  Test pq_query_db ##############################

class CheckQuery(unittest.TestCase):
    def setUp(self):
        self.quant_list = [
            [2., 4., 6., 8., 10., 12.],
            [1., 3., 5., 7., 9., 11.]
        ]

    def check_nominal(self, averaged, calculated):
        for list1, list2 in zip(calculated, averaged):
            for val1, val2 in zip(list1, list2):
                self.assertAlmostEquals(unc.nominal_value(val1), val2, 5)

    def check_stdev(self, averaged, calculated):
        for list1, list2 in zip(calculated, averaged):
            for val1, val2 in zip(list1, list2):
                self.assertAlmostEquals(unc.std_dev(val1), val2, 5)

    def test_average_columns_nominal(self):
        averaged = [[5.0, 7.0, 11.0],
                    [5.0, 6.0, 10.0]
                    ]
        series = [0, 0, 60, 60, 120, 120]
        stages = [0, 60, 120]
        # noinspection PyArgumentEqualDefault
        calculated = average_columns(self.quant_list, series, stages, cutoff=5)[0]
        print calculated
        self.check_nominal(averaged, calculated)

    def test_average_columns_stdev(self):
        averaged = [[0, 1.0, 1.0],
                    [0, 1.0, 1.0]
                    ]
        series = [0, 0, 60, 60, 120, 120]
        stages = [0, 60, 120]
        # noinspection PyArgumentEqualDefault
        calculated = average_columns(self.quant_list, series, stages, cutoff=5)[0]
        self.check_stdev(averaged, calculated)

    def test_average_columns_nominal_shuffled(self):
        averaged = [[6.0, 4.0, 11.0],
                    [5.0, 3.0, 10.0]
                    ]
        series = [60, 0, 60, 0, 120, 120]
        stages = [0, 60, 120]
        calculated, _ = average_columns(self.quant_list,
                                        series, stages, cutoff=0)

        self.check_nominal(averaged, calculated)

    def test_average_columns_stdev_shuffled(self):
        averaged = [[2.0, 2.0, 1.0],
                    [2.0, 2.0, 1.0]
                    ]
        series = [60, 0, 60, 0, 120, 120]
        stages = [0, 60, 120]
        calculated, _ = average_columns(self.quant_list,
                                        series, stages, cutoff=0)

        self.check_stdev(averaged, calculated)

    def test_get_ratios_nominal(self):
        averaged = [[3.0, 7.0, 11.0],
                    [2.0, 6.0, 10.0]
                    ]
        ratios = [[2.33333, 3.666666],
                  [3.0, 5.0]]

        stages = [0, 60, 120]
        my_flags = [[0, 0, 0],
                    [0, 1, 0]
                    ]

        calculated, _ = get_ratios(averaged, stages, my_flags)

        self.check_nominal(calculated, ratios)

    def test_get_stages_in_experiment(self):
        series = [[0, 0, 60, 60, 120, 120],
                  [0, 60, 120, 120, 0, 60],
                  [120, 0, 60, 120, 60, 0]]

        expected = [0, 60, 120]
        for row in series:
            result = get_stages_in_experiment(row)
            for val1, val2 in zip(result, expected):
                self.assertEquals(val1, val2)

# ########################  Test pq_fill_tables ##############################

class CheckFill(unittest.TestCase):
    def test_set_p_amino_in_consensus(self):
        consensus_mod = ["I(737)DASK(737)GLSNS(23)SPRJ",
                         "I(737)DASK(737)GLSNS(21)JSPR",
                         "I(737,737)DASK(737)JGLSNS(23)SPR",
                         "I(737)DASKJ(737)GLSNS(737,23)SPR",
                         "I(723)JDASK(737)GLSNS(23)SPR",
                         "J(723)DASK(737)GLSNS(231)SPR",
                         ]

        consensus = ["IDASKGLSNSSPRJ",
                     "IDASKGLSNSJSPR",
                     "IDASKJGLSNSSPR",
                     "IDASKJGLSNSSPR",
                     "IJDASKGLSNSSPR",
                     "JDASKGLSNSSPR",
                     ]

        expected = ["IDASKGLSNsSPRJ",
                    "IDASKGLSNsJSPR",
                    "IDASKJGLSNsSPR",
                    "IDASKJGLSNsSPR",
                    "IJDASKGLSNsSPR",
                    "JDASKGLSNSSPR",
                    ]
        for mod, consensus, sequence in zip(consensus_mod, consensus, expected):
            self.assertEquals(sequence, set_p_amino_in_consensus(mod, consensus))
#
#
#
#
#
if __name__ == "__main__":
    unittest.main()
